/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import java.util.HashMap;

class CheckTreeAndData {

    boolean isFine = true;
    boolean isDone = false;
    String error = new String("");

    CheckTreeAndData(String[] treeNodes, HashMap seqs) {
	
	ProAlign.log("CheckTreeAndData");

	for(int i=0; i<treeNodes.length; i++) {
	    isDone = true;
	    if(!seqs.containsKey(treeNodes[i])) {
		isFine = false;
		ProAlign.log.println("CheckTreeAndData: "+treeNodes[i]+": no sequence found!");
		error = ""+treeNodes[i]+": no sequence found!";
	    } 
	}
    }
    
    boolean nodesAreSame() {
	if(isDone) {
	    return isFine;
	} else {
	    return false;
	}
    }
    
    String getError() {
	return error;
    }
}
