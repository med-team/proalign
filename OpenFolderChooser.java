/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import javax.swing.JFileChooser;
import java.awt.Component;
import java.awt.Font;
import java.io.File;
import java.io.IOException;

public class OpenFolderChooser extends JFileChooser {

    Component parent; 
    String txt;

    OpenFolderChooser(Component parent, String txt) { 
	this.parent = parent;
	this.txt = txt;
    }

    String openFile() {
	
	String filepath = new String("");
	JFileChooser jfc = new JFileChooser(ProAlign.folderPath);
	jfc.setFileSelectionMode(1);

	int returnValue = jfc.showDialog(parent, txt);

	if(returnValue == JFileChooser.APPROVE_OPTION) {
	    File path;
	    if (jfc.getCurrentDirectory().toString().endsWith(File.separator)) {
		path = new File(jfc.getCurrentDirectory().toString()
				+ jfc.getSelectedFile().getName());
	    } else {
		path = new File(jfc.getCurrentDirectory().toString()
				+ File.separator + jfc.getSelectedFile().getName());
	    }
	    filepath = path.toString();
	}
	return filepath;

    }
}

