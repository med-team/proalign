/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

class TransformLog {

    TransformLog() { }
    double sumLogs(double a, double b) {
	if(Double.isNaN(a) && Double.isNaN(b)) {
	    return Double.NEGATIVE_INFINITY;
	}
	else if(Double.isNaN(a)){
	    return b;
	} 
	else if(Double.isNaN(b)){
	    return a;
	} 
	if(b>a){
	    double c = a;
	    a = b; 
	    b = c;
	}
	return (a+Math.log(1+Math.exp(b-a)));
    }
}



