/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import java.util.HashMap;
import java.util.Iterator;

public class CheckSequence {

    String error = new String("");
    String alphabet;
    CheckSequence() { 
	ProAlign.log("CheckSequence");
    }

    boolean isDna(HashMap seqs) {
	
	Iterator seqKeys = seqs.keySet().iterator();	
	String name, seq;
	int acgt = 0, total = 0;
	
	while(seqKeys.hasNext()) {
	    name = (String) seqKeys.next();
	    seq = (String) seqs.get(name);
	    
	    for(int i=0; i<seq.length(); i++) {
		if(seq.charAt(i)=='A') {
		    acgt++;
		} else if(seq.charAt(i)=='C') {
		    acgt++;
		} else if(seq.charAt(i)=='G') {
		    acgt++;
		} else if(seq.charAt(i)=='T') {
		    acgt++;
		} else if(seq.charAt(i)=='U') {
		    acgt++;
		}
		total++;
	    }
	}
	if(((double) acgt/(double) total)>0.9d) {
	    return true;
	}
	return false;
    }

    boolean isFromAlphabet(HashMap seqs,String alphabet) {

	boolean isFine = true;
	Iterator seqKeys = seqs.keySet().iterator();	
	String name, seq;

	while(seqKeys.hasNext()) {
	    name = (String) seqKeys.next();
	    seq = (String) seqs.get(name);
	    
	    for(int i=0; i<seq.length(); i++) {
		if(alphabet.indexOf(seq.charAt(i))<0) {
		    isFine = false;
		    error += "\n "+name+": '"+seq.charAt(i)+
			"' is not from alphabet '"+alphabet+"'!";
		    ProAlign.log.print("\n "+name+": '"+seq.charAt(i)+
				       "' is not from alphabet '"+alphabet+"'!");	
		} 
	    }
	}
	return isFine;
    }

    String getError() {
	return error;
    }
}
