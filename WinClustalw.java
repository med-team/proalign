/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

public class WinClustalw extends Thread {

  String command;
  int num;
  boolean running = true;

  WinClustalw(int num, String command) { 

      this.num = num;
      this.command = command;
      start();
  }

  public void run(){
    runClustalw(num,command);
    running = false;
  }
    
  /**
   * Run native ClustalW
   */
  public native void runClustalw(int num, String cmnd);
  static {
      Runtime.getRuntime().load(ProAlign.clustalwPath);
  }
  
  /**
   * Get output and write it in the log.
   */
  private void writeout(String txt) {
      //System.out.println(txt);
      ProAlign.log(txt);
    try {
	sleep(20);
    } catch(InterruptedException e) {}
  }
}






