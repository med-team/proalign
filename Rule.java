/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import javax.swing.JComponent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;

public class Rule extends JComponent {
    int rulerHeight = 15;
    
    private int increment;
    private int startPosition;
    private int units;

    Rule rule;
    ResultWindow parent;
    public Rule(ResultWindow rw) { 
	
	rule = this;
	parent = rw; 
	
	addMouseListener(new MiceListener());
    }

    public void setIncrement(int columnWidth, int startPosition) {
	increment = columnWidth;
	this.startPosition = startPosition;
	units = 10*columnWidth;
    }

    public void setPreferredWidth(int pw) {
	setPreferredSize(new Dimension(pw,rulerHeight));
    }

    public void paintComponent(Graphics g) {
	Rectangle drawHere = g.getClipBounds();
	
	// Fill clipping area 
	g.setColor(Color.white);
	g.fillRect(drawHere.x, drawHere.y, drawHere.width, drawHere.height);

	// Do the ruler labels in a small font 
	g.setFont(new Font(ProAlign.paFontName, Font.PLAIN, ProAlign.paFontSize-2)); 
	g.setColor(Color.black);
	
	// Some vars we need.
	int tickLength = 0;
	String text = null;
	
	// Use clipping bounds to calculate first tick and last tick location.
	int start = (drawHere.x / increment) * increment;
	int end = (((drawHere.x + drawHere.width) / increment) + 1) * increment;
	
	// Make a special case of 0 to display the number
	// within the rule and draw a units label.
	if (start == 0) {
	    text = Integer.toString(0);
	    tickLength = 3;
	    g.drawLine(0, rulerHeight-1, 0, rulerHeight-tickLength-1);
	    start = increment;
	}
	
	// ticks and labels
	for (int i = start; i < end; i += increment) {
	    if (i % units == 0)  {
		tickLength = 3;
		text = Integer.toString(i/increment);
	    } else {
		tickLength = 1;
		text = null;
	    }
	    
	    if (tickLength != 0) { 
		g.drawLine(i, rulerHeight-1, i, rulerHeight-tickLength-1);
		if (text != null)
		    g.drawString(text, i-10, 10);
	    }
	}
    }
    // A click on a site will include/exclude that character.
    //
    class MiceListener extends MouseAdapter {
	public void mouseClicked(MouseEvent e) {
	    int x = e.getX();
	    int y = e.getY();
	    if(e.isShiftDown()) {
		parent.seqcont.updateStableSites(x,y,true);
	    } else {
		parent.seqcont.updateStableSites(x,y,false);
	    }	
	}
    } 


}






