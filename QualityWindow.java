/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.BorderFactory;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

public class QualityWindow extends JFrame {

    JScrollPane sPane;
    int height;
    double[] postProb;

    PixelRule pixRule;
    PrintCurve curve;
    ResultWindow rw;
    String name;

    QualityWindow(AlignmentNode root, String name, ResultWindow rw) {

	this.rw = rw;
	this.name = name;

	postProb = new double[root.cellPath.length];
	for(int i=0; i<root.cellPath.length; i++) {
	    postProb[i] = root.getOnePostProbAt(i, name);
	}
	height = this.getHeight();

	curve = new PrintCurve(QualityWindow.this, postProb);

	// pixelruler.
	pixRule = new PixelRule();
	pixRule.setPreferredWidth((int) curve.getPreferredSize().getWidth());
	pixRule.setIncrement(curve.xScale);
	pixRule.setBackground(Color.white);


	sPane = new JScrollPane(curve,
				 JScrollPane.VERTICAL_SCROLLBAR_NEVER,
				 JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);


	sPane.setViewportBorder(BorderFactory.createLineBorder(Color.black));
	sPane.setColumnHeaderView(pixRule);
	sPane.setBackground(Color.white);

	Container cp = getContentPane();
	cp.add(sPane);

	addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e){
		    PrintTree.numOpenWindows--;
		    dispose();
		}
	    });
    }

    int getVisibleHeight() {
	return sPane.getViewport().getHeight();
    }

    void upDateData(AlignmentNode root, String name){
	postProb = new double[root.cellPath.length];
	for(int i=0; i<root.cellPath.length; i++) {
	    postProb[i] = root.getOnePostProbAt(i, name);
	}
	curve.upDateData(postProb);
    }
}







