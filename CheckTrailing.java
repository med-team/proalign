/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

class CheckTrailing {

    PwAlignment pwa;

    CheckTrailing(ProAlign pa) { 

	ProAlign.log("CheckTrailing");

	PwSubstitutionMatrix psm = new PwSubstitutionMatrix();
	String pwAlphabet;
	int[][] pwSubst;
	int gOpen, gExt;
	
	if(ProAlign.isDna){
	    pwAlphabet = psm.dnaAlphabet;
	    pwSubst = psm.swdna;
	    gOpen = -1*pa.pwDnaOpen;
	    gExt = -1*pa.pwDnaExt;
	    
	} else {
	    
	    pwAlphabet = psm.protAlphabet;
	    if(pa.pwProtMatrix.equals("pam60")) {
		pwSubst = psm.pam60;
	    } else if(pa.pwProtMatrix.equals("pam160")) {
		pwSubst = psm.pam160;
	    } else if(pa.pwProtMatrix.equals("pam250")) {
		    pwSubst = psm.pam250;
	    } else {
		pwSubst = psm.pam120;
	    }
	    gOpen = -1*pa.pwProtOpen;
	    gExt = -1*pa.pwProtExt;
	}
	
	pwa = new PwAlignment(pwSubst,gOpen,gExt,pwAlphabet,ProAlign.isDna);
    }

    int[] trailing(String s1, String s2) {	
	return pwa.trailing(s1,s2);
    }

}
