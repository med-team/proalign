/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import java.awt.Component;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import javax.swing.JTextArea;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.JOptionPane;
import javax.swing.JDialog;
import java.awt.Color;

public class OpenDialog extends JDialog {

    Font font = new Font(ProAlign.paFontName, Font.PLAIN, ProAlign.paFontSize);
    Component parent;
    
    OpenDialog(Component parent) {
	this.parent = parent;
    }

    void showDialog(String title, String text) {

	JTextArea ta = new JTextArea(text);
	ta.setFont(font);
	ta.setEditable(false);
	ta.setBackground(Color.white);
		
	Border loweredbevel = BorderFactory.createLoweredBevelBorder();
	ta.setBorder(loweredbevel);
		
	JOptionPane op = new JOptionPane();
	op.setMessage(ta);
	op.setMessageType(JOptionPane.PLAIN_MESSAGE);
		
	JDialog dialog = op.createDialog(parent,title);
	dialog.setLocation(150,150);
	dialog.show();
    }
}


