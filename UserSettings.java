/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import java.io.File;
import java.io.IOException;

/**   
 *  Reads and writes a file for user settings
 */
public class UserSettings {

    String filepath;
    File file;

    UserSettings() {

	ProAlign.log("UserSettings");

	if(System.getProperty("os.name").startsWith("Windows")){
	    filepath = "proalign.ini";
	} else {
	    filepath = ".proalignrc";
	}
	file = new File(filepath);
    }

    // Read existing settings
    String[] readSettings() {
	String[] userdata = new String[3];
	String str = new String();
	if(file.exists()) {
	    try {
		InFile dataIn = new InFile(filepath);
		if((str = dataIn.readLine())!=null) {
		    if(new File(str).isDirectory()){
			userdata[0] = str; 
		    }
		}
		if((str = dataIn.readLine())!=null) {
		    if(new File(str).isFile()){
			userdata[1] = str; 
		    }
		}
		if((str = dataIn.readLine())!=null) {
		    if(new File(str).isDirectory()){
			userdata[2] = str; 
		    }
		}
		dataIn.close();
	    } catch (IOException ie) { }  
	}
	return userdata;
    }
    
    // Write current settings
    void writeSettings(String[] userdata) {
	if(file.exists()) {
	    try {
		file.delete();
	    } catch (Exception ie) { }  
	}
	try {  
	    OutFile dataOut = new OutFile(filepath);
	    dataOut.println(userdata[0]); // ProAlign data
	    dataOut.println(userdata[1]); // ProAlign ClustalW
	    dataOut.println(userdata[2]); // temp
	    dataOut.close();
	} catch (IOException ie) { }  
    }
}






