/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

public class ReadArguments {
    
    ReadArguments(ProAlign pa, String[] args) {

	ProAlign.log("ReadArguments");
	
	for(int i=0; i<args.length; i++) {

	    // sequence file
	    if(args[i].startsWith("-seqfile=")) {
		pa.seqfile = args[i].substring(args[i].indexOf("=")+1);
		ProAlign.log.println(" sequence file: "+pa.seqfile);

	    // guide tree file
	    } else if(args[i].startsWith("-treefile=")) {
		pa.treefile = args[i].substring(args[i].indexOf("=")+1);
		ProAlign.log.println(" tree file: "+pa.treefile);

	    // no GUI		
	    } else if(args[i].startsWith("-nogui")) {
		pa.nogui = true;
		ProAlign.log.println(" no GUI: true");

	    // do tree
	    } else if(args[i].startsWith("-newtree")) {
		pa.doTree = true;
		ProAlign.log.println(" do tree: true");

	    // sample from post. probabilities 		
	    } else if(args[i].startsWith("-sample")) {
		pa.trackBest = false;
		ProAlign.log.println(" sample backtrace path: true");

	    // 	gap frequency
	    } else if(args[i].startsWith("-gapfreq=")) {
		if(args[i].endsWith("=estimate")) {
		    pa.estimateGapFreq = true;
		    pa.estimateParameters = true ;
		    ProAlign.log.println(" gap frequency: estimate");
		} else {
		    double tmp = new Double(
			args[i].substring(args[i].indexOf("=")+1)).doubleValue();
		    if(tmp>0d && tmp<0.5d) {
			pa.gapFreq = tmp;
			ProAlign.log.println(" gap frequency: "+tmp);
		    } else {
			ProAlign.log.println(" bad value: gap frequency "+tmp); 
		    }
		}

	    // 	gap probability
	    } else if(args[i].startsWith("-gapprob=")) {
		if(args[i].endsWith("=estimate")) {
		    pa.estimateGapProb = true;
		    pa.estimateParameters = true ;
		    ProAlign.log.println(" gap probability: estimate");
		} else {
		    double tmp = new Double(
			args[i].substring(args[i].indexOf("=")+1)).doubleValue();
		    if(tmp>0d && tmp<0.5d) {
			pa.gapProb = tmp;
			ProAlign.log.println(" gap probability: "+tmp);
		    } else {
			ProAlign.log.println(" bad value: gap probability "+tmp); 
		    }
		}

	    // 	delta for HMM model		
	    } else if(args[i].startsWith("-delta=")) {
		if(args[i].endsWith("=estimate")) {
		    pa.estimateDelta = true;
		    pa.estimateParameters = true ;
		    ProAlign.log.println(" HMM delta: estimate");
		} else {
		    double tmp = new Double(
			args[i].substring(args[i].indexOf("=")+1)).doubleValue();
		    if(tmp>0d && tmp<0.5d) {
			pa.modelDelta = tmp;
			ProAlign.log.println(" HMM delta: "+tmp);
		    } else {
			ProAlign.log.println(" bad value: HMM delta "+tmp); 
		    }
		}

	    // 	epsilon for HMM model		
	    } else if(args[i].startsWith("-epsilon=")) {
		if(args[i].endsWith("=estimate")) {
		    pa.estimateEpsilon = true;
		    pa.estimateParameters = true ;
		    ProAlign.log.println(" HMM epsilon: estimate");
		} else {		
		    double tmp = new Double(
			args[i].substring(args[i].indexOf("=")+1)).doubleValue();
		    if(tmp>0d && tmp<1.0d) {
			pa.modelEpsilon = tmp;
			ProAlign.log.println(" HMM epsilon: "+tmp);
		    } else {
			ProAlign.log.println(" bad value: HMM epsilon "+tmp); 
		    }
		}

	    // 	band width		
	    } else if(args[i].startsWith("-bwidth=")) {
		int tmp = new Integer(
		    args[i].substring(args[i].indexOf("=")+1)).intValue();
		if(tmp>5) {
		    pa.bandWidth = tmp;
		    ProAlign.log.println(" band width: "+tmp);
		} else {
		    ProAlign.log.println(" bad value: band width "+tmp); 
		}

	    // 	distance scale
	    } else if(args[i].startsWith("-distscale=")) {
		double tmp = new Double(
		    args[i].substring(args[i].indexOf("=")+1)).doubleValue();
		if(tmp>0d && tmp<2.0d) {
		    pa.distScale = tmp;
		    ProAlign.log.println(" distance scale: "+tmp);
		} else {
		    ProAlign.log.println(" bad value:  distance scale "+tmp); 
		}
	    // multiple hits 
	    } else if(args[i].startsWith("-nocorrection")) {
		pa.correctMultiple = false ;
		ProAlign.log.println(" correctMultiple: "+pa.correctMultiple);

	    // terminal gaps
	    } else if(args[i].startsWith("-penalize")) {
		if(args[i].endsWith("=true")) {
		    pa.penalizeTerminal = true ;
		} else if(args[i].endsWith("=false")) {		
		    pa.penalizeTerminal = false ;
		}
		ProAlign.log.println(" penalizeTerminal: "+pa.penalizeTerminal);

	    // mean post prob.
	    } else if(args[i].startsWith("-writemean")) {
		pa.writeMean = true ;
		ProAlign.log.println(" output mean: "+pa.writeMean);

	    // all post prob.
	    } else if(args[i].startsWith("-writeall")) {
		pa.writeAll = true ;
		ProAlign.log.println(" output all: "+pa.writeAll);

	    // root characters
	    } else if(args[i].startsWith("-writeroot")) {
		pa.writeRoot = true ;
		ProAlign.log.println(" output root: "+pa.writeRoot);

	    // protein model
	    } else if(args[i].startsWith("-wag")) {
		ProAlign.protModel = "wag";
		ProAlign.isDna = false;
		ProAlign.log.println(" protein model: "+ProAlign.protModel);

	    } else if(args[i].startsWith("-dayhoff")) {
		ProAlign.protModel = "dayhoff";
		ProAlign.isDna = false;
		ProAlign.log.println(" protein model: "+ProAlign.protModel);

	    } else if(args[i].startsWith("-jtt")) {
		ProAlign.protModel = "jtt";
		ProAlign.isDna = false;
		ProAlign.log.println(" protein model: "+ProAlign.protModel);

	    // trailing 
	    } else if(args[i].startsWith("-notrailing")) {
		pa.removeTrailing = false ;
		ProAlign.log.println(" removeTrailing: "+pa.removeTrailing);


	    // 	allowed trailing 
	    } else if(args[i].startsWith("-trailing=")) {
		int tmp = new Integer(
		    args[i].substring(args[i].indexOf("=")+1)).intValue();
		if(tmp>5 && tmp < pa.bandWidth/2) {
		    pa.offset = tmp;
		    ProAlign.log.println(" trailing: "+tmp);
		} else {
		    ProAlign.log.println(" bad value: trailing "+tmp); 
		}

	    // output file
	    } else if(args[i].startsWith("-outfile=")) {
		pa.outfile = args[i].substring(args[i].indexOf("=")+1);
		ProAlign.log.println(" output file: "+pa.outfile);

	    // output format
	    } else if(args[i].startsWith("-outformat=")) {
		if(args[i].substring(args[i].indexOf("=")+1).equalsIgnoreCase("fasta")){
		    pa.outformat = 2;
		    ProAlign.log.println(" outformat: PIR");
		} else if(args[i].substring(args[i].indexOf("=")+1).equalsIgnoreCase("PIR")){
		    pa.outformat = 2;
		    ProAlign.log.println(" outformat: PIR");
		} else if(args[i].substring(args[i].indexOf("=")+1).equalsIgnoreCase("nexus")){
		    pa.outformat = 1;
		    ProAlign.log.println(" outformat: nexus");
		} else if(args[i].substring(args[i].indexOf("=")+1).equalsIgnoreCase("phylip")){
		    pa.outformat = 3;
		    ProAlign.log.println(" outformat: phylip");
		} else if(args[i].substring(args[i].indexOf("=")+1).equalsIgnoreCase("msf")){
		    pa.outformat = 4;
		    ProAlign.log.println(" outformat: msf");
		}
		
	    // 	stay quiet, no log 
	    } else if(args[i].startsWith("-quiet")) {
		pa.DEBUG = false;
		ProAlign.log.println(" quiet...");
		ProAlign.log.flush();
	    } else {
		ProAlign.log.println("Unrecognized paramter: "+args[i]);
		ProAlign.log.flush();
	    }		
	}
    }
}
