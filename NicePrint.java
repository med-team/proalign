/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

class NicePrint {
    NicePrint() { };
    String dbl(double number, int length) {
	return ((""+number+"          ").substring(0,length+1));
    }

    String rdbl(double val, int prec) {
        String full = ""+val;
        if(full.indexOf('.')>-1) {
            String begin = full.substring(0,full.indexOf('.'));
            String end = full.substring(full.indexOf('.')+1);
            if(end.length()>prec) {
		String zeros = new String();
		while(end.startsWith("0")) {
		    end = end.substring(1);
		    zeros += "0";
		}
                char num = end.charAt(prec);
                if(num=='0'||num=='1'||num=='2'||num=='3'||num=='4') {
                    full = begin+"."+zeros+end.substring(0,prec);
                } else {
                    full = begin+"."+zeros+(new Integer(end.substring(0,prec)).intValue()+1);
                }
            }
        }
        return full;
    }
    
    void pdbl(double number, int length) {
	System.out.print((""+number+"          ").substring(0,length+1));
    }
    void str(String str) {
	System.out.print(str);
    }
    void strl(String str) {
	System.out.println(str);
    }
}
