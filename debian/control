Source: proalign
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Thorsten Alteholz <debian@alteholz.de>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               javahelper,
               default-jdk
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/proalign
Vcs-Git: https://salsa.debian.org/med-team/proalign.git
Homepage: http://loytynojalab.biocenter.helsinki.fi/software/proalign/
Rules-Requires-Root: no

Package: proalign
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         default-jre
Description: Probabilistic multiple alignment program
 ProAlign performs probabilistic sequence alignments using hidden Markov
 models (HMM). It includes a graphical interface (GUI) allowing to (i)
 perform alignments of nucleotide or amino-acid sequences, (ii) view the
 quality of solutions, (iii) filter the unreliable alignment regions and
 (iv) export alignments to other software.
 .
 ProAlign uses a progressive method, such that multiple alignment is
 created stepwise by performing pairwise alignments in the nodes of a
 guide tree. Sequences are described with vectors of character
 probabilities, and each pairwise alignment reconstructs the ancestral
 (parent) sequence by computing the probabilities of different
 characters according to an evolutionary model.
