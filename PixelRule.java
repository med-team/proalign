/**
 * Title:        ProAlign<p>
 * Description:  sequence alignment comparison program<p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import java.awt.Toolkit;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import javax.swing.JComponent;

// A nice ruler for the quality window.
//
public class PixelRule extends JComponent {
  int rulerHeight = 15;

  private int increment;
  private int startPosition = 0;
  private int units;

  public PixelRule() {  }

  public void setIncrement(int xScale) {
    increment = xScale;
    units = 25*xScale;
  }
  
  public void setPreferredWidth(int pw) {
    setPreferredSize(new Dimension(pw,rulerHeight));
  }

  public void paintComponent(Graphics g) {
    Rectangle drawHere = g.getClipBounds();

    // Fill clipping area 
    g.setColor(Color.white);
    g.fillRect(drawHere.x, drawHere.y, drawHere.width, drawHere.height);

    // Do the ruler labels in a small font 
    g.setFont(new Font(ProAlign.paFontName, Font.PLAIN, ProAlign.paFontSize-2)); 
    g.setColor(Color.black);
    
    // Some vars we need.
    int tickLength = 0;
    String text = null;
    
    // Use clipping bounds to calculate first tick and last tick location.
    int start = (drawHere.x / increment) * increment;
    int end = (((drawHere.x + drawHere.width) / increment) + 1) * increment;
    
    // Make a special case of 0 to display the number
    // within the rule and draw a units label.
    if (start == 0) {
      text = Integer.toString(0);
      tickLength = 3;
      g.drawLine(0, rulerHeight-1, 0, rulerHeight-tickLength-1);
      g.drawString("0", 0, 10);
      start = increment;
    }

    // ticks and labels
    for (int i = start; i < end; i += increment) {
	if (i % units == 0)  {
	  tickLength = 3;
	  text = Integer.toString(i/increment);
	} else if(i % 10 == 0)  {
	    tickLength = 1;
	    text = null;
	} else {
	    tickLength = 0;
	    text = null;
	}
      
	if (tickLength != 0) { 
	    g.drawLine(i, rulerHeight-1, i, rulerHeight-tickLength-1);
	    if (text != null)
		g.drawString(text, i-10, 10);
	}
    }
  }
}












