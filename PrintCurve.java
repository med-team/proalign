/**
 * Title:        ProAlign<p>
 * Description:  sequence alignment comparison program<p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
 
//  Print a curve on a JPanel.
// 
public class PrintCurve extends JPanel {

    double[] postProb;
    int height = 100;
    int xScale = 2; // scale: pixels per column.
    PrintCurve pc;
    QualityWindow qw;

    PrintCurve(QualityWindow qw, double[] postProb) {
	pc = this;
	this.qw = qw;
	this.postProb = postProb;

	setBackground(Color.white);
	setForeground(Color.black);

	setPreferredSize(new Dimension(xScale*postProb.length, height));
	addMouseListener(new MiceListener());
    }

    // Update data when changing node. 
    //
    void upDateData(double[] postProb) {
	this.postProb = postProb;
	pc.updateUI();
    }

    public void paintComponent(Graphics g) {
	Graphics2D  g2 = (Graphics2D) g;
	super.paintComponent(g2);      //clears the background
	height = pc.qw.getVisibleHeight();
	pc.setPreferredSize(new Dimension(xScale*postProb.length, height));
	for(int i=0; i<postProb.length-1; i++) {
	    int y1 = (int) (Math.exp(postProb[i])*height);
	    int y2 = (int) (Math.exp(postProb[i+1])*height);
	    if(y1>0 && y2>0) {
		g2.drawLine(i*xScale,height-y1,(i+1)*xScale,height-y2);
	    }
	}
    }

    // Listens mouse click: focus alignment, update messageText. 
    //
    class MiceListener extends MouseAdapter {
	public void mouseClicked(MouseEvent e) {
	    int x = e.getX();
	    int y = e.getY();
	    qw.rw.focusAlignment(x/xScale);
	    qw.rw.convertNodeInfoX(x/xScale,qw.name);
	}
    }
}

