/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import java.io.File;
import javax.swing.filechooser.FileFilter;

public class PAFileFilter extends FileFilter {

    // Accept all directories and files ending with ProAlign.fileExt . 
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
	
	String s = f.getName();
	int i = s.lastIndexOf('.');
	String ext = new String();
        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
	
	if (ext != null) {
            if (ext.equals(ProAlign.fileExt)) {
		return true;
            } else {
                return false;
            }
	}
	
        return false;
    }
    
    // The description of this filter
    public String getDescription() {
        return "ProAlign Object-file";
    }
}
