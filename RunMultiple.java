/**
 * Title:        ProAlign<p>
 * Description:  <p>
 * Copyright:    Copyright (c) Ari Loytynoja<p>
 * License:      GNU GENERAL PUBLIC LICENSE<p>
 * @see          http://www.gnu.org/copyleft/gpl.html
 * Company:      ULB<p>
 * @author Ari Loytynoja
 * @version 1.0
 */
package proalign;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.io.DataInputStream;
import java.io.BufferedInputStream;

/**
 * Running alignment in a separate thread
 */
public class RunMultiple extends Thread {

    ResultWindow rw;

    RunMultiple(ResultWindow rw) {
	
	this.rw = rw;

	ProAlign.log("RunMultiple");

	start();
    }

    public void run() {

	boolean isError = false;
	String errorMsg = new String();

	try {
	    rw.root.align();
	} catch(TraceBackException tbe) { 
	    isError = true;
	    errorMsg = tbe.getMessage();
	} catch(Exception e) { }

	if(isError) {
	    
	    OpenDialog od = new OpenDialog(rw);
	    od.showDialog("Error!", "\n  "+errorMsg+"\n  Try to increase the band width.\n");

	    rw.file[1].setEnabled(true); // open alignment
	    rw.file[2].setEnabled(false); // save alignment
	    rw.data[0].setEnabled(true); // import data
	    rw.data[1].setEnabled(true); // import guide tree
	    rw.align[0].setEnabled(true); // do guide tree  NOT YET
	    rw.align[1].setEnabled(true); // do multiple
	    rw.align[2].setEnabled(true); // do complete  NOT YET
	    rw.align[3].setEnabled(true); // sample
	    rw.align[4].setEnabled(false); // remove gaps
	    
	    rw.dm.setEnabled(true); // import
	    rw.em.setEnabled(false); // export 

	} else {
	    
	    rw.setAlignedData(rw.root);
	    rw.setScrollPanes();
	    
	    if(rw.root.isBandWarning()) {
		String text = new String(
		    "\n  Traceback path came very close\n  to the band edge."+
		    " Alignment may\n  not be the correct one!");
		OpenDialog od = new OpenDialog(rw);
		od.showDialog("Warning!",text);
	    }
	    
	    if(!rw.root.isUnique()) {
		String text = new String(
		    "\n  There exist more than one possible\n  alignment. The backtrace "+
		    "path was\n  chosen "+rw.root.getSampleTimes()+" times randomly between\n"+
		    "  two equally good alternatives.\n");
		OpenDialog od = new OpenDialog(rw);
		od.showDialog("Notice!",text);	 
	    }
	}
    }
}















